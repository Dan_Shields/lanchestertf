<?php

class User {
	protected $_userRegistered;
	protected $_id;
	protected $_steamId;
	protected $_userInfo;
	protected $_userTeam;
	protected $_userInvites;

	public static function fromID($userid) {
		$instance = new static();
		$instance->loadByID($userid);
		return $instance;
	}

	public static function fromSteam($steamid) {
		$instance = new static();
		$instance->loadBySteam($steamid);
		return $instance;
	}

	protected function loadByID($userid) {
		require_once 'database.php';
		global $dbh;

		$sql = "SELECT * FROM `tbl-user` WHERE `user_id` = :userid";
		$userResult = $dbh->select($sql,['userid' => $userid]);
		
		if ($userResult) {
			$this->_userRegistered = TRUE;
			$this->fillInfo($userResult);
		} else {
			$this->_userRegistered = FALSE;
		}
		
	}	

	protected function loadBySteam($steamid) {
		require_once 'database.php';
		global $dbh;

		$sql = "SELECT * FROM `tbl-user` WHERE `user_steamid` = :steamid";
		$userResult = $dbh->select($sql,['steamid' => $steamid]);
		
		if ($userResult) {
			$this->_userRegistered = TRUE;
			$this->fillInfo($userResult);
		} else {
			$this->_userRegistered = FALSE;
		}
	}

	protected function fillInfo($userResult) {
		require_once 'database.php';
		global $dbh;

		$userData = $userResult[0];

		$this->_id = $this->_userInfo['id'] = $userData['user_id'];

		$this->_steamid = $this->_userInfo['steamid'] = $userData['user_steamid'];

		$this->_userInfo['first_name'] = $userData['user_first_name'];
		$this->_userInfo['last_name'] = $userData['user_last_name'];
		$this->_userInfo['alias'] = $userData['user_alias'];
		$this->_userInfo['email'] = $userData['user_email'];
		$this->_userInfo['registration'] = $userData['registration_time'];
		$this->_userInfo['profile_pic_url'] = $userData['user_profile_pic_url'];

		$this->fillTeam();

		$this->fillInvites();

	}

	protected function fillTeam() {
		global $dbh;

		$teamSQL = "SELECT * FROM `tbl-user-team` ut INNER JOIN `tbl-team` t ON ut.`user-team_team_id` = t.`team_id` WHERE `user-team_user_id`= :userid";
		$team = $dbh->select($teamSQL,['userid' => $this->_id]);
		

		if (!$team) {
			$this->_userTeam = FALSE;
		} else {
			$userTeam = [];

			$team = $team[0];

			$userTeam['id'] = $team['team_id'];
			$userTeam['name'] = $team['team_name'];
			$userTeam['creation_time'] = $team['team_creation_time'];
			$userTeam['ready'] = $team['team_ready'];
			$userTeam['user_role'] = $team['user-team_role'];

			$this->_userTeam = $userTeam;
		}
	}

	protected function fillInvites() {
		global $dbh;

		$inviteSQL = "SELECT * FROM `tbl-user-invite` ui INNER JOIN `tbl-team` u ON ui.`invite_team_id` = u.`team_id` WHERE `invite_user_id` = :userid";
		$invites = $dbh->select($inviteSQL, ['userid' => $this->_id]);

		if (!$invites) {
			$this->_userInvites = FALSE;
		} else {
			$userInvites = [];

			foreach ($invites as $invite) {
				$invite['invite_id'] = $invite['invite_id'];

				$userInvites[] = $invite;
			}

			$this->_userInvites = $userInvites;
		}
	}

	//GETTERS
	public function getID() {
		return $this->_id;
	}

	public function getSteam() {
		return $this->_steamId;
	}

	public function getInfo() {
		return $this->_userInfo;
	}

	public function registered() {
		return $this->_userRegistered;
	}

	public function getTeam() {
		return $this->_userTeam;
	}

	public function getInvites() {
		return $this->_userInvites;
	}
}

class LoggedInUser extends User {
	public function __construct($steamid) {
		$this->_steamid = $steamid;
		$this->loadBySteam($steamid);
	}

	public function register($userInfo) {;
		if (!$this->registered()) {
			require_once 'database.php';
			global $dbh;

			$sql = "INSERT INTO `tbl-user` (`user_steamid`, `user_first_name`, `user_last_name`, `user_alias`, `user_email`, `user_profile_pic_url`) VALUES (:steamid, :first_name, :last_name, :alias, :email, :profile_pic_url)";
			$result = $dbh->change($sql,$userInfo);

			return $result;
		} else {
			return FALSE;
		}
	}

	public function update() {
		if ($this->registered()) {
			require_once 'database.php';
			require_once 'steamauth/userInfo.php';
			global $dbh;

	        $sql = 'SELECT TIME_TO_SEC(TIMEDIFF(NOW(), `user_profile_update_time`)) as diff FROM `tbl-user` WHERE user_id = :userid';
	       	$result = $dbh->select($sql,['userid' => $this->_id]);
	    
	   		//if more than 5 minutes old refresh url and reset timestamp
	   		$time = $result[0]['diff'];
	   		if ($time >= 300) {
	           	$sql = "UPDATE `tbl-user` SET `user_profile_pic_url`=:profile_pic_url, `user_profile_update_time`=NOW() WHERE `user_id`=:userid";
	           	$dbh->change($sql,['userid' => $this->_id, 'profile_pic_url' => substr($steamprofile['avatarmedium'], 72, 40)]);
	   		}
	   		return TRUE;
		} else {
			return FALSE;
		}
	}	
}



?>