<?php

function displayTeam($team) {
	$teamInfo = $team->getInfo();
	$teamMembers = $team->getMembers();

	$_SESSION['last_viewed_team'] = $team->getID();

	echo 'Team Name: ' . $teamInfo['name'] . '.<br/>';
	echo 'Created on: ' . $teamInfo['creation_time'] . '.<br/>';
	echo '<br/>';

	if ($teamMembers) {
		
		$isLeader = TRUE;

		foreach ($teamMembers as $teamMember) {
			$name = $teamMember['first_name'] . " '<a href='profile?id=" . $teamMember['id'] . "'><b>" . $teamMember['alias'] . "</b></a>' " . $teamMember['last_name'];

			if ($isLeader) {
				echo "Leader: ". $name .".";
				echo "<br/>";
				echo 'Members:<ul>';
				$isLeader = FALSE;
			} else {
				echo "<li>" . $name . ".</li>";
			}
			
		}
		echo '</ul>';
	} else {

	}
	
}

function manageTeam($team) {

	$teamInfo = $team->getInfo();
	$teamMembers = $team->getMembers();

	$_SESSION['last_viewed_team'] = $team->getID();

	$deleteButton = "<form action='php/manage-team.php' method='post'><input type='hidden' name='teamid' value='". $teamInfo['id'] ."'/><button name='delete' type='submit'>Delete Team</button></form>";

	echo $deleteButton . "<br/>";

	echo 'Team Name: ' . $teamInfo['name'] . '.<br/>';
	echo 'Created on: ' . $teamInfo['creation_time'] . '.<br/>';
	echo '<br/>';

	if ($teamMembers) {
		foreach ($teamMembers as $teamMember) {
			$name = $teamMember['first_name'] . " '<a href='profile?id=" . $teamMember['id'] . "'><b>" . $teamMember['alias'] . "</b></a>' " . $teamMember['last_name'];

			if ($teamMember['role'] == 1) {
				echo "Leader: ". $name .".";
				echo "<br/>";
				if (count($teamMembers) > 1) {
					echo 'Members:<ul>';
				}
			} else {
				echo "<li>" . $name . ". - ";
				$team->manageUser($teamMember['id']);
				echo "</li>";
			}
			
		}
		echo '</ul>';
	}
}

?>