<?php
require 'database.php';
require 'team-classes.php';
require 'user-classes.php';
require '../steamauth/steamauth.php';

if (!isset($_SESSION['userid'])) {
	die('must be logged in and registered');
}

if (!(isset($_POST['team_name']) && !$_POST['team_name'] == "")) {
	die('Parameter error.');
}

$teamInfo['name'] = $_POST['team_name'];
$teamInfo['leader'] = $_SESSION['userid'];

$newTeam = new NewTeam($teamInfo);

if ($newTeam->success() === TRUE) {
	header('Location: ../team?id='. $newTeam->getID());
} else {
	die('Team Creation failed');
}

?>