<?php
require 'database.php';
require 'team-classes.php';
require 'user-classes.php';
require '../steamauth/steamauth.php';

if (!isset($_SESSION['steamid'])) {
	die('must be logged in');
}

if (!(isset($_POST['teamid']) && is_numeric($_POST['teamid']) && $_POST['teamid'] > 0) || !(isset($_POST['promote']) || isset($_POST['kick']) || isset($_POST['delete']) || isset($_POST['invite']))) {
	die('Parameter error.');
}

$teamID = $_POST['teamid'];

$team = new Team($teamID);

if (!$team->isLeader($_SESSION['userid'])) {
	die('You must be team leader to manage this team.');
}

if (isset($_POST['delete'])) {
	if (!$team->delete()) {
		die('Delete failed');
	} else {
		header('Location: ../profile');
	}
	
} else if (isset($_POST['userid']) && is_numeric($_POST['userid']) && $_POST['userid'] > 0) {

	$userID = $_POST['userid'];

	if (isset($_POST['promote'])) {

		if ($team->getLeader()['id'] === $userID) {
			die("You cannot promote yourself.");
		}

		if (!$team->promote($userID)) {
			die("Promote failed");
		}

	} else if (isset($_POST['kick'])) {

		if ($team->getLeader()['id'] === $userID) {
			die("You cannot kick yourself.");
		}

		if (!$team->kick($userID)) {
			die('Kick failed');
		}
	} else if (isset($_POST['invite'])) {

		if ($team->getLeader()['id'] === $userID) {
			die("You cannot invite yourself.");
		}

		if (!$team->invite($userID)) {
			die('Invite failed');
		}
	}

	header('Location: ../team?id='. $team->getID());
} else {
	die('UserID not specified');
}



?>