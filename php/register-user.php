<?php
	require 'database.php';
	require 'user-classes.php';
	require '../steamauth/steamauth.php';

	if (isset($_SESSION['steamid']) && isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email']) && isset($_POST['alias'])) {
		require '../steamauth/userInfo.php';

		$newUser = new LoggedInUser($steamprofile['steamid']);

		$newUserInfo = [];

		$newUserInfo['steamid'] = $_SESSION['steamid'];
		$newUserInfo['first_name'] = htmlspecialchars(strip_tags($_POST['first_name']));
		$newUserInfo['last_name'] = htmlspecialchars(strip_tags($_POST['last_name']));
		$newUserInfo['email'] = htmlspecialchars(strip_tags($_POST['email']));
		$newUserInfo['alias'] = htmlspecialchars(strip_tags($_POST['alias']));
		$newUserInfo['profile_pic_url'] = substr($steamprofile['avatarmedium'], 72, 40);

		if ($newUser->register($newUserInfo)) {
			header('Location: http://localhost/lan.site');
		} else {
			die('registration error');

		}
	} else {
		die('Parameter error');
	}

?>