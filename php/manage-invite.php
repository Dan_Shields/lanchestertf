<?php
require 'database.php';
require 'team-classes.php';
require 'user-classes.php';
require '../steamauth/steamauth.php';

if (!isset($_SESSION['steamid'])) {
	die('Must be logged in');
}

echo $_POST['teamid'];

if (isset($_SESSION['userid']) && isset($_POST['teamid']) && is_numeric($_POST['teamid']) && $_POST['teamid'] > 0) {

	$user = User::fromID($_SESSION['userid']);
	$teamid = $_POST['teamid'];

	$team = new Team($teamid);

	if ($team->exists() === FALSE) {
		die("Team doesn't exist.");
	}

	$invites = $user->getInvites();

	if (!$invites === FALSE) {
		foreach ($invites as $invite) {
			if ($invite['team_id'] === $teamid) {
				global $dbh;

				if (isset($_POST['accept'])) {
					$joinSQL = "INSERT INTO `tbl-user-team`(`user-team_user_id`, `user-team_team_id`) VALUES (:userid, :teamid)";
					$joinResult = $dbh->change($joinSQL, ['userid' => $user->getID(), 'teamid' => $teamid]);

					$removeSQL = "DELETE FROM `tbl-user-invite` WHERE `invite_user_id` = :userid AND `invite_team_id` = :teamid";
					$removeResult = $dbh->change($removeSQL, ['userid' => $user->getID(), 'teamid' => $teamid]);

					if ($joinResult && $removeResult) {
						header('Location: ../profile.php?id='. $_SESSION['userid']);
					} else {
						die('accept failed');
					}
				} else if (isset($_POST['decline'])) {
					$removeSQL = "DELETE FROM `tbl-user-invite` WHERE `invite_user_id` = :userid AND `invite_team_id` = :teamid";
					$removeResult = $dbh->change($removeSQL, ['userid' => $user->getID(), 'teamid' => $teamid]);

					if ($removeResult) {
						header('Location: ../profile.php?id='. $_SESSION['userid']);
					} else {
						die('decline failed');
					}
					
				} else {
					die('Not action specified.');
				}

			} else {
				die("Invite doesn't exist");
			}
		}

	} else {
		die('User has no invites');
	}

} else {
	die('Parameter error.');
}


?>