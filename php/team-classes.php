<?php

class Team {
	protected $_teamExists;
	protected $_id;
	protected $_teamInfo;
	protected $_teamMembers;

	public function __construct($teamID) {
		require_once 'database.php';
		global $dbh;

		$sql = "SELECT * FROM `tbl-team` WHERE `team_id` = :teamid";
		$result = $dbh->select($sql,['teamid' => $teamID]);

		if (!$result) {
			$this->_teamExists = FALSE;
		} else {
			$this->_teamExists = TRUE;
			$this->fillInfo($result);
		}
	}

	protected function update() {
		require_once 'database.php';
		global $dbh;

		$sql = "SELECT * FROM `tbl-team` WHERE `team_id` = :teamid";
		$result = $dbh->select($sql,['teamid' => $this->_id]);

		if (!$result) {
			$this->_teamExists = FALSE;
		} else {
			$this->_teamExists = TRUE;
			$this->fillInfo($result);
		}
	}

	private function fillInfo($teamResult) {
		$teamData = $teamResult[0];

		$this->_id = $this->_teamInfo['id'] = $teamData['team_id'];

		$this->_teamInfo['name'] = $teamData['team_name'];

		$this->_teamInfo['creation_time'] = $teamData['team_creation_time'];

		$this->_teamInfo['ready'] = $teamData['team_ready'];

		require_once 'database.php';
		global $dbh;

		$sql = "SELECT * FROM `tbl-user-team` ut INNER JOIN `tbl-user` u ON ut.`user-team_user_id` = u.`user_id` WHERE `user-team_team_id`= :teamid ORDER BY `user-team_role` DESC";
		$result = $dbh->select($sql,['teamid' => $this->_id]);

		if (!$result) {
			$this->_teamMembers = FALSE;
		} else {
			$this->fillMembers($result);
		}
	}

	private function fillMembers($membersResult) {
		$teamMembers = [];
		$i = 1;
		
		foreach($membersResult as $teamMember) {
			$member['id'] = $teamMember['user_id'];
			$member['steamid'] = $teamMember['user_steamid'];
			$member['first_name'] = $teamMember['user_first_name'];
			$member['last_name'] = $teamMember['user_last_name'];
			$member['alias'] = $teamMember['user_alias'];
			$member['email'] = $teamMember['user_email'];
			$member['registration_time'] = $teamMember['registration_time'];
			$member['profile_pic_url'] = $teamMember['user_profile_pic_url'];
			$member['join_time'] = $teamMember['user-team_creation_time'];
			$member['role'] = $teamMember['user-team_role'];

			//Ensures that the first element in the array is the leader of the team
			if ($teamMember['user-team_role'] == 1) {
				$teamMembers[0] = $member;
			} else {
				$teamMembers[$i] = $member;
				$i++;
			}
		}

		$this->_teamMembers = $teamMembers;
	}

	public function manageUser($userID) {
		if ($this->_teamExists && $this->isMember($userID)) {
			echo "<form action='php/manage-team.php' method='post'><input type='hidden' name='userid' value='". $userID ."'/><input type='hidden' name='teamid' value='". $this->_id ."'/><button name='promote' type='submit'>Promote</button></form>";
			echo "<form action='php/manage-team.php' method='post'><input type='hidden' name='userid' value='". $userID ."'/><input type='hidden' name='teamid' value='". $this->_id ."'/><button name='kick' type='submit'>Kick</button></form>";
		}
	}

	public function kick($userID) {
		if ($this->isMember($userID)) {
			require_once 'database.php';
			global $dbh;

			$sql = "DELETE FROM `tbl-user-team` WHERE `user-team_user_id` = :userid AND `user-team_team_id` = :teamid";
			$result = $dbh->change($sql,['userid' => $userID, 'teamid' => $this->_id]);

			if ($result) {
				$this->update();
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function promote($userID) {
		if ($this->isMember($userID)) {
			require_once 'database.php';
			global $dbh;

			$leaderID = $this->getLeader()['id'];

			$removeLeaderSQL = "UPDATE `tbl-user-team` SET `user-team_role` = 0 WHERE `user-team_user_id`=:leaderid";
			$addLeaderSQL = "UPDATE `tbl-user-team` SET `user-team_role` = 1 WHERE `user-team_user_id`=:promotionid";

			$removeResult = $dbh->change($removeLeaderSQL, ['leaderid' => $leaderID]);
			$addResult = $dbh->change($addLeaderSQL, ['promotionid' => $userID]);

			if ($removeResult && $addResult) {
				$this->update();
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function invite($userID) {
		if (!$this->isMember($userID)) {
			$user = User::fromID($userID);

			if ($user->getTeam() !== FALSE) {
				die("Cannot invite user because they are already in the team '". $this->_teamInfo['name'] ."'.");
			}

			global $dbh;

			$inviteCheckSQL = "SELECT `invite_id` FROM `tbl-user-invite` WHERE `invite_team_id` = :teamid AND `invite_user_id` = :userid";
			$check = $dbh->select($inviteCheckSQL, ['teamid' => $this->_id, 'userid' => $userID]);

			if ($check) {
				die('User already invited');
			} else {
				$inviteSQL = "INSERT INTO `tbl-user-invite`(`invite_team_id`, `invite_user_id`) VALUES (:teamid, :userid)";

				$inviteResult = $dbh->change($inviteSQL, ['teamid' => $this->_id, 'userid' => $userID]);

				if ($inviteResult) {
					return TRUE;
				} else {
					return FALSE;
				}
			}

		} else {
			die('User already in team.');
		}
	}

	public function delete() {
		global $dbh;

		$kickSQL = "DELETE FROM `tbl-user-team` WHERE `user-team_team_id` = :teamid";
		$deleteSQL = "DELETE FROM `tbl-team` WHERE `team_id` = :teamid";

		$kickResult = $dbh->change($kickSQL, ['teamid' => $this->_id]);
		$deleteResult = $dbh->change($deleteSQL, ['teamid' => $this->_id]);

		if ($kickResult && $deleteResult) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	//GETTERS
	public function getInfo() {
		return $this->_teamInfo;
	}

	public function getID() {
		return $this->_id;
	}

	public function getMembers() {
		return $this->_teamMembers;
	}

	public function exists() {
		return $this->_teamExists;
	}

	public function getLeader() {
		return $this->_teamMembers[0];
	}

	public function isMember($userID) {
		foreach ($this->_teamMembers as $teamMember) {
			if ($teamMember['id'] == $userID) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function isLeader($userID) {
		if ($this->_teamMembers[0]['id'] == $userID) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

class NewTeam {
	protected $_id;
	protected $_success;

	public function __construct($teamInfo) {
		$leader = User::fromID($_SESSION['userid']);
		$userTeam = $leader->getTeam();

		if ($userTeam !== FALSE) {
			die('Cannot be in two teams.');
		}

		global $dbh;

		//Creates the team in `tbl-team`
		$createSQL = "INSERT INTO `tbl-team`(`team_id`, `team_name`) VALUES (NULL, :name)";
		$createResult = $dbh->change($createSQL, ['name' => $teamInfo['name']]);

		//Finds the ID of the newly created team
		$idSQL = "SELECT `team_id` FROM `tbl-team` WHERE `team_id` = LAST_INSERT_ID()";
		$this->_id = $dbh->select($idSQL, [])[0]['team_id'];

		//Inserts the row of the leader into `tbl-user-team`
		$leaderSQL = "INSERT INTO `tbl-user-team`(`user-team_user_id`, `user-team_team_id`, `user-team_role`) VALUES (:leaderid, :teamid, 1)";
		$leaderResult = $dbh->change($leaderSQL, ['leaderid' => $teamInfo['leader'], 'teamid' => $this->_id]);

		if ($createResult === FALSE || $leaderResult === FALSE) {
			$this->_success = FALSE;
		} else {
			$this->_success = TRUE;
		}
	}

	//GETTERS
	public function getID() {
		return $this->_id;
	}

	public function success() {
		return $this->_success;
	}
}


?>