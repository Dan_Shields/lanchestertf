<?php

function displayUser($user) {
	$userInfo = $user->getInfo();
	$userTeams = $user->getTeams();

	$title = $userInfo['first_name'] . " '" . $userInfo['alias'] . "' " . $userInfo['last_name'];

	$profile_pic_url = expandPPURL($userInfo['profile_pic_url']);

	echo "<img src='" . $profile_pic_url . "'></img>";
	echo '<br/>';
	echo 'Name: ' . $title;
	echo '<br/>';
	echo 'User since: ' . $userInfo['registration'];
	echo '<br/>';

	if ($userTeam !== FALSE) {
		echo "Team: ";
		echo "<a href='team?id=" . $userTeam['id'] . "'>" . $userTeam['name'] . "</a>.";
		echo '<br/>';
	}

	if (isset($_SESSION['userid'])) {
		global $loggedInUser;

		if ($loggedInUser->registered() && $loggedInUser->getID() != $user->getID()) {

			$ownedTeams = $loggedInUser->getOwnedTeams();

			if(!$ownedTeams === FALSE) {

				echo "
				<form action='php/manage-team.php' method='post'>
					<input type='hidden' value='". $user->getID() ."' name='userid'></input>
					<select name='teamid'>
						<option></option>";


				foreach ($ownedTeams as $team) {
					echo "<option value='" . $team['id'] . "'>". $team['name'] ."</option>";
				}

				echo "
					</select>
					<button name='invite' type='submit'>Invite</button>
				</form>";

			}
		} else if ($loggedInUser->registered() && $loggedInUser->getID() === $user->getID()) {
			$invites = $loggedInUser->getInvites();

			if (!$invites === FALSE) {
				echo "Team Invites:<br/>";

				echo "<ul>";

				foreach ($invites as $invite) {
					echo "<li>";

					echo "<a href='team.php?id=" . $invite['team_id'] . "'>";
					echo $invite['team_name'] ."</a> ";
					inviteResponseButtons($invite['team_id']);

					echo "</li>";
				}
			}
		}
	}
}

function expandPPURL($shorturl) {
	$profile_pic_url = 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fa/' . $shorturl . '_medium.jpg';
	return $profile_pic_url;
}

function inviteResponseButtons($teamID) {
	echo "
	<form action='php/manage-invite.php' id='invite-response' method='post'>
		<input type='hidden' name='teamid' value='". $teamID ."'/>
		<button name='accept' type='submit'>Accept</button>
		<button name='decline' type='submit'>Decline</button>
	</form>";
}

?>