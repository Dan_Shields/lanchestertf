-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2016 at 08:39 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lan`
--
CREATE DATABASE IF NOT EXISTS `lan` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lan`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl-blog`
--

DROP TABLE IF EXISTS `tbl-blog`;
CREATE TABLE `tbl-blog` (
  `post_id` int(11) NOT NULL,
  `post_writer` int(11) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_title` varchar(128) NOT NULL,
  `post_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl-team`
--

DROP TABLE IF EXISTS `tbl-team`;
CREATE TABLE `tbl-team` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(32) NOT NULL,
  `team_creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `team_game` enum('TF2','CSGO','Dota 2','LoL','Rocket League') NOT NULL,
  `team_ready` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl-team`
--

INSERT INTO `tbl-team` (`team_id`, `team_name`, `team_creation_time`, `team_game`, `team_ready`) VALUES
(1, 'Best team eu', '2016-12-10 12:10:12', 'TF2', 0),
(2, 'Even better than you LoL!', '2016-12-10 12:50:10', 'Dota 2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl-user`
--

DROP TABLE IF EXISTS `tbl-user`;
CREATE TABLE `tbl-user` (
  `user_id` int(11) NOT NULL,
  `user_steamid` char(17) NOT NULL,
  `user_first_name` varchar(32) NOT NULL,
  `user_last_name` varchar(32) NOT NULL,
  `user_alias` varchar(32) NOT NULL,
  `user_email` varchar(64) NOT NULL,
  `registration_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_profile_pic_url` char(128) NOT NULL,
  `user_profile_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl-user`
--

INSERT INTO `tbl-user` (`user_id`, `user_steamid`, `user_first_name`, `user_last_name`, `user_alias`, `user_email`, `registration_time`, `user_profile_pic_url`, `user_profile_update_time`) VALUES
(5, '40191981098', 'Adam', 'Shields', 'Adam', 'dasdfa@agfasdg.com', '2016-11-22 19:21:11', '', '2016-11-22 19:21:11'),
(10, '76561198040699398', 'Daniel', 'Shields', 'DanS', 'd.shieldsuk@gmail.com', '2016-11-27 19:23:13', 'd07c178c9c6507416084f14722600e3277779fb6', '2016-12-10 20:21:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl-user-team`
--

DROP TABLE IF EXISTS `tbl-user-team`;
CREATE TABLE `tbl-user-team` (
  `user-team_id` int(11) NOT NULL,
  `user-team_user_id` int(11) NOT NULL,
  `user-team_team_id` int(11) NOT NULL,
  `user-team_role` tinyint(1) NOT NULL DEFAULT '0',
  `user-team_creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl-user-team`
--

INSERT INTO `tbl-user-team` (`user-team_id`, `user-team_user_id`, `user-team_team_id`, `user-team_role`, `user-team_creation_time`) VALUES
(1, 10, 1, 0, '2016-12-10 12:10:13'),
(2, 10, 2, 1, '2016-12-10 12:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl-user-ticket`
--

DROP TABLE IF EXISTS `tbl-user-ticket`;
CREATE TABLE `tbl-user-ticket` (
  `ticket_id` int(11) NOT NULL,
  `ticket_user_id` int(11) NOT NULL,
  `ticket_payment_reference` varchar(64) NOT NULL,
  `ticket_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl-blog`
--
ALTER TABLE `tbl-blog`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `tbl-team`
--
ALTER TABLE `tbl-team`
  ADD PRIMARY KEY (`team_id`),
  ADD UNIQUE KEY `team_name` (`team_name`);

--
-- Indexes for table `tbl-user`
--
ALTER TABLE `tbl-user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `user_steamid` (`user_steamid`),
  ADD UNIQUE KEY `email` (`user_email`),
  ADD UNIQUE KEY `user_alias` (`user_alias`);

--
-- Indexes for table `tbl-user-team`
--
ALTER TABLE `tbl-user-team`
  ADD PRIMARY KEY (`user-team_id`),
  ADD KEY `user-team_user_id` (`user-team_user_id`,`user-team_team_id`),
  ADD KEY `team_id` (`user-team_team_id`);

--
-- Indexes for table `tbl-user-ticket`
--
ALTER TABLE `tbl-user-ticket`
  ADD PRIMARY KEY (`ticket_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl-blog`
--
ALTER TABLE `tbl-blog`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl-team`
--
ALTER TABLE `tbl-team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl-user`
--
ALTER TABLE `tbl-user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl-user-team`
--
ALTER TABLE `tbl-user-team`
  MODIFY `user-team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl-user-ticket`
--
ALTER TABLE `tbl-user-ticket`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl-user-team`
--
ALTER TABLE `tbl-user-team`
  ADD CONSTRAINT `team_id` FOREIGN KEY (`user-team_team_id`) REFERENCES `tbl-team` (`team_id`),
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user-team_user_id`) REFERENCES `tbl-user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
