<?php
require 'php/database.php';
require 'steamauth/steamauth.php';
require_once 'php/user-functions.php';
require_once 'php/user-classes.php';

?>

<!DOCTYPE html>
<html lang=en-uk>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LANchester 2017</title>

	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

  	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  	<link rel="stylesheet" type="text/css" href="css/bootflat.css">
  	<link rel="stylesheet" type="text/css" href="css/main.css">

  	<link rel="shortcut icon" href="img/favicon.ico">
  	
</head>

<body>

<header>
	<div class='container' id='header-container'>
	<div id='navigation'>
		<a><img id='banner' src='img/banner-tagline.png'></a>
		<nav>
		<ul class='nav nav-pills'>
			<li role="presentation" class="active"><a href="#">Home</a></li>
 		 	<li role="presentation"><a href="#">Event</a></li>
 			<li role="presentation"><a href="#">Teams</a></li>
 		</ul>
		</nav>
	</div>
	<ul class='profile'>
	<?php
	if (!isset($_SESSION['steamid'])) {
		echo loginButton('rectangle');
	} else {
		require 'steamauth/userinfo.php';

		$loggedInUser = new LoggedInUser($steamprofile['steamid']);


		if ($loggedInUser->registered()) {
			$userInfo = $loggedInUser->getInfo();
	?>

	<li class='profile-img'>
	<?php
		echo "<a href='profile?id=" . $loggedInUser->getID() . "'>";

	?>

	<img src=<?php echo expandPPURL($userInfo['profile_pic_url']); ?>></a></li>
	<li class='profile-text'><h4>Hi 
	<?php
	

	echo $userInfo['first_name'];

	echo "</a>.";

	?>

	</h3></li>

	<?php
	} else {
		echo "Hi <b>". $steamprofile['personaname'] . "</b>, please register <a href='register'>here</a>.";
	}
	echo logoutButton();

}

	?>
		
	</ul>

	</div>
</header>

<section id='content'>
	<div class='container'>

	</div>

</section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>