<?php
require 'php/database.php';
require 'steamauth/steamauth.php';
require_once 'php/user-classes.php';

$shouldRegister = FALSE;

if (!isset($_SESSION['steamid'])) {
	echo loginbutton('rectangle');
} else {
	require 'steamauth/userinfo.php';

	$loggedInUser = new LoggedInUser($steamprofile['steamid']);

	if ($loggedInUser->registered()) {
		echo "<a href='http://localhost/lan.site/profile?id=" . $loggedInUser->getID() . "'>My Profile</a>";
	} else {
		echo "Hi <b>". $steamprofile['personaname'] . "</b>, please register below.";
		$shouldRegister = TRUE;
	}
	echo logoutButton();
}
if ($shouldRegister) {
	echo "
	<form id='user-registration' action='php/register-user.php' method='post'>

		First Name: 	<input type='text' id='first_name' name='first_name'><br/>
		Last Name: 		<input type='text' id='last_name' name='last_name'><br/>
		E-mail: 		<input type='text' id='email' name='email'><br/>
		Desired Alias: 	<input type='text' id='alias' name='alias'><br/>

	<input type='submit'>
	</form>";
}


?>