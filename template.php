<!DOCTYPE html>
<html lang=en-uk>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LANchester 2017</title>

	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

  	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	
  	<link rel="stylesheet" type="text/css" href="css/bootflat.css">
  	<link rel="stylesheet" type="text/css" href="css/main.css">

  	<link rel="shortcut icon" href="img/favicon.ico">
</head>

<body>

<header>
	<div class='container' id='header-container'>
	<div id='navigation'>
		<a><img id='banner' src='img/banner-tagline.png'></a>
		<nav>
		<ul class='nav nav-pills'>
			<li role="presentation" class="active"><a href="#">Home</a></li>
 		 	<li role="presentation"><a href="#">Event</a></li>
 			<li role="presentation"><a href="#">Teams</a></li>
 		</ul>
		</nav>
	</div>
	<ul class='profile'>
		<li class='profile-img'><img src='img/test-profile.jpg'></li>
		<li class='profile-text'><h4>Hi Barry</h3></li>
		<li class='profile-text'><h6>Yo!</h6></li>
	</ul>

	</div>
</header>

<section id='content'>
	<div class='container'>

	</div>

</section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>