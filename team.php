<?php
require 'php/database.php';
require 'steamauth/steamauth.php';
require_once 'php/user-classes.php';
require_once 'php/team-classes.php';
require_once 'php/team-functions.php';

if (!isset($_SESSION['steamid'])) {
	echo loginButton('rectangle');
	echo "<br/>";
} else {
	require 'steamauth/userinfo.php';

	$loggedInUser = new LoggedInUser($steamprofile['steamid']);

	if ($loggedInUser->registered()) {
		echo "<a href='http://localhost/lan.site/profile?id=" . $loggedInUser->getID() . "'>My Profile</a>";
		$userInfo = $loggedInUser->getInfo();
	} else {
		echo "Hi <b>". $steamprofile['personaname'] . "</b>, please register <a href='register'>here</a>.";
	}
	echo logoutButton();
}

if (isset($_GET['create'])) {

	echo "
	<form id='team-creation' action='php/create-team.php' method='post'>

		Team Name: 	<input type='text' id='team_name' name='team_name'><br/>
	</form>";

} else if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
	$specifiedTeam = new Team($_GET['id']);

	if (!$specifiedTeam->exists()) {
		echo "<br/>";
		echo "Team not found. Please check the team id and try again.";
	} else {
		$leaderInfo = $specifiedTeam->getLeader();

		if ($leaderInfo['id'] == $loggedInUser->getID()) {
			echo "You lead this team.";
			echo '<br/>';
			manageTeam($specifiedTeam);
		} else {
			displayTeam($specifiedTeam);
		}

	}
} else {
	echo "Please specify a team to view by navigating to team/team_id.";
}




?>