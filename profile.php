<?php
require 'php/database.php';
require 'steamauth/steamauth.php';
require_once 'php/user-classes.php';
require_once 'php/user-functions.php';

if (isset($_SESSION['steamid'])) {
	require 'steamauth/userinfo.php';

	$loggedInUser = new LoggedInUser($steamprofile['steamid']);

	if ($loggedInUser->registered()) {
		echo "<a href='http://localhost/lan.site/profile?id=" . $loggedInUser->getID() . "'>My Profile</a>";
	} else {
		echo "Hi <b>". $steamprofile['personaname'] . "</b>, please register <a href='register'>here</a>.";
	}
	echo logoutButton();

	//prioritise 'id' specified in GET, else show logged in user info
	if (isset($_GET['id'])) {
		$specifiedUser = User::fromID($_GET['id']);
		if (!$specifiedUser->registered()) {
			echo "<br/>";
			echo "User not found. Please check your userid and try again.";
		} else {
			displayUser($specifiedUser);
		}
	} else if ($loggedInUser->registered()) {
		displayUser($loggedInUser);
	} else {
		echo "Please register to view your profile or view someone else's by going to profile/their_id.";
	}
} else {
	echo loginButton('rectangle');
	echo '<br/>';

	if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
		$specifiedUser = User::fromID($_GET['id']);
		if (!$specifiedUser->registered()) {
			echo "<br/>";
			echo "User not found. Please check your userid and try again or login to view your own profile.";
		} else {
			displayUser($specifiedUser);
		}
	}
}
?>